#include <Arduino.h>
#include <Arduino_LSM9DS1.h>
#include <math.h>

void setup() {
	Serial.begin(9600);
	while (!Serial);
	Serial.println("Started");

	if (!IMU.begin()) {
		Serial.println("Failed to initialize IMU!");
		while (1);
	}
	Serial.print("Magnetic field sample rate = ");
	Serial.print(IMU.magneticFieldSampleRate());
	Serial.println(" uT");
	Serial.println();
	Serial.println("Magnetic Field in uT");
	Serial.println("X\tY\tZ");
}

void loop() {
	float x, y, z;
	if (IMU.magneticFieldAvailable()) {
		IMU.readMagneticField(x, y, z);

		Serial.print("x: ");
		Serial.print(x);
		Serial.print("\ty: ");
		Serial.print(y);
		Serial.print("\tz: ");
		Serial.println(z);

		float heading = 180 * atan2(y,x)/PI;
		if(heading < 0)
        	heading += 360;
		
		Serial.print("Heading: ");
		Serial.println(heading, 3);
		delay(500);
	}
}